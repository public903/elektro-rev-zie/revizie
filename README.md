# Revízie


## Vybavenie

- ohmmeter, na meranie prechodových odporov uzemnenia PE

- merák RCD

- vidlicový klúč 13/17, 10

- šroubováky ploché, krížové izolované

- izolačná páska

- wago svorky

- konbinačky

- malý štetec na prach do rozvádzačov


## Označovanie

- HR - hlavný rozvádzač
- RS - svetelný rozvádzač
- DT - rozvádzač meranie a regulácia

## Postupy

### Rozvádzač RS

- prívodný kábel + istenie 
  - zistiť prierez kábla a porovnať s dokumentáciou a s istením
  - prívodný kábel môže mať o tri rády menší prierez ako je jeho istenie na začiatku, ak je požadované istenie zabezpečené na konci prívodu

- podoťahovať šrouby na všetkých svorkovniciach

- odmerať prúdový chránič

- odmerať impedanciu prívodu

### Izba

- lampy, kávovar, kanvica, fén - skontrolovať opticky kábel, opálenie vidlice a zásuvky a odmerať PE ak je zariadenie triedy I

- kúpelňa - zmerať pospojovanie batérií a všetkých dostupných kovových častí

- zásuvky - zmerať impedanciu všetkých zásuviek


